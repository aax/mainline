#!/bin/bash

# Script to convert PDF file to JPG images
#
# Dependencies:
# * pdftk
# * imagemagick

PDF=$1

echo "Processing $PDF"
DIR=`basename "$1" .pdf`
NUMPAGES=`pdftk "${PDF}" dump_data output | grep -i NumberOfPages | cut -d' ' -f2`

mkdir -p "$DIR"
rm -rf *.html
rm -rf *.txt
rm -rf "$DIR"/*.html

echo '  Splitting PDF file to pages...'
#pdftk "$PDF" burst output "$DIR"/$DIR-%04d.pdf
#pdftk "$PDF" dump_data output "$DIR"/metadata.txt

echo '  Converting pages to JPEG files...'
#for i in "$DIR"/*.pdf; do
#  convert -colorspace RGB -interlace none -density 300x300 -quality 100 "$i" "$DIR"/`basename "$DIR-$i" .pdf`.jpg
#  convert -colorspace RGB -interlace none -density 72x72 -quality 80 "$i" "$DIR"/`basename "$DIR-$i" .pdf`-low.jpg
#  convert -colorspace RGB -interlace none -density 16x16 -quality 60 "$i" "$DIR"/`basename "$DIR-$i" .pdf`-thumb.jpg
#done

echo ' Making index pages of issue '

echo "<!DOCTYPE html><html><head><title>" > $DIR.html
echo $DIR >> $DIR.html
echo "</title>" >> $DIR.html
echo "<link rel='stylesheet' href='http://www.w3.org/StyleSheets/Core/Chocolate' type='text/css'>" >> $DIR.html
echo "<style>ul {margin: 0; padding: 0;} li {list-style: none; display: inline;}</style>" >> $DIR.html
echo "</head><body>" >> $DIR.html
echo "<h1>" >> $DIR.html
echo $DIR >> $DIR.html
echo "</h1><ul>" >> $DIR.html

for i in "$DIR"/*.pdf; do
    ISS=`basename "$i" .pdf`
    ISSUE=${ISS%-*}
    NEXT=$((10#${ISS#*-} + 1))
    PREV=$((10#${ISS#*-} - 1))

    echo "<li><a href="$ISS.html"><img src="$DIR/$ISS-thumb.jpg" alt="$ISS" /></a></li>" >> $DIR.html

#echo "</ul><hr /><address>&copy; 1985-1997, Future Publishing</address></body></html>" >> $DIR.html

    echo "<!DOCTYPE html><html><head><title>" > $ISS.html
    echo $ISS >> $ISS.html
    echo "</title>" >> $ISS.html
    echo "<link rel='stylesheet' href='http://www.w3.org/StyleSheets/Core/Chocolate' type='text/css'>" >> $ISS.html
    echo "<style>td, .c {text-align: center;}</style>" >> $ISS.html
    echo "</head><body>" >> $ISS.html
    echo "<h1>" >> $ISS.html
    echo $ISS >> $ISS.html
    echo "</h1>" >> $ISS.html
    echo "<table style='width: 100%;'><tr><td width='33%'>" >> $ISS.html
    if [[ $PREV -gt 0 ]]; then
        echo "<a href=\"$ISSUE-`printf "%04d" $PREV`.html\">Previous.</a>" >> $ISS.html
    else
        echo "&nbsp;" >> $ISS.html
    fi
    echo "</td><td width='33%'><form method='get' action='$DIR/$ISS.jpg'><p class='c'><input type='submit' value='Full-size scan' /></p></form><td width='33%'>" >> $ISS.html
    if [[ $NEXT -le $NUMPAGES ]]; then
        echo "<a href=\"$ISSUE-`printf "%04d" $NEXT`.html\">Next.</a>" >> $ISS.html
    else
        echo "&nbsp;" >> $ISS.html
    fi
    echo "</td></tr></table>" >> $ISS.html
    echo "<p class='c'><img src="$DIR/$ISS-low.jpg" alt="$ISS" /></a></p>" >> $ISS.html
    echo "<table style='width: 100%;'><tr><td width='33%'>" >> $ISS.html
    if [[ $PREV -gt 0 ]]; then
        echo "<a href=\"$ISSUE-`printf "%04d" $PREV`.html\">Previous.</a>" >> $ISS.html
    else
        echo "&nbsp;" >> $ISS.html
    fi
    echo "</td><td width='33%'><form method='get' action='$DIR/$ISS.jpg'><p class='c'><input type='submit' value='Full-size scan' /></p></form><td width='33%'>" >> $ISS.html
    if [[ $NEXT -le $NUMPAGES ]]; then
        echo "<a href=\"$ISSUE-`printf "%04d" $NEXT`.html\">Next.</a>" >> $ISS.html
    else
        echo "&nbsp;" >> $ISS.html
    fi
    echo "</td></tr></table>" >> $ISS.html
    echo "<h2>OCR output of this page</h2>" >> $ISS.html

    rm $ISS.txt
    pdftohtml -s -i -stdout $DIR/$ISS.pdf
    html2text -ascii $DIR/$ISS-html.html > $ISS.txt
    markdown $ISS.txt >> $ISS.html

    echo "<hr /><address>&copy; 1985-1997, Future Publishing</address></body></html>" >> $ISS.html

done

rm -rf "$DIR"/*.html
rm -rf *.txt

echo 'All done'
